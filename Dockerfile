FROM ubuntu:latest

# Requirements
RUN    apt-get update \
    && apt-get install -y git curl \
    && rm -rf /var/lib/apt/lists/*

# kubectl, kustomize and helm
RUN cd /tmp \
    && curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
    && mv ./kubectl /usr/local/bin/ \
    && chmod a+x /usr/local/bin/kubectl \
    && curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash \
    && mv ./kustomize /usr/local/bin/ \
    && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

RUN mkdir /workspace

WORKDIR /workspace
